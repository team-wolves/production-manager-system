# Production Manager System




Team wolves is of 4 members, Ravindu Perera, Pasindu Wijesena, Hirantha Rathnayake and Thilara Ekanayake. We are first year undergraduates of University of Colombo School of Computing following the computer science course.  

We’ve come up with a scalable solution for the Production Manager System, developed fo SLIIT Codefest Hackathon 2018.

For data ingestion we’ve used tools such as Apache Flume and Docker. Apache Flume is a distributed, reliable, and available software for efficiently collecting, aggregating, and moving large amounts of log data. Docker is a container platform which allows developers to build, ship and run distributed applications. 

We’ve also used Libraries such as Google Gson, Apache Flume Sdk, Apache CSVParser in data ingestion.

Data processing is done using aggregate queries which allows the database to be clustered and processes when processing large data sets.

For data presentation we’ve used a framework of Angular, responsive angular dashboard. 

The database that we’ve used in this solution is MongoDB which fits big data. It’s more scalable and flexible with data.  

