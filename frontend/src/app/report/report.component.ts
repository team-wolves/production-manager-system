import { Component, OnInit } from '@angular/core';
import { IotLine } from '../iot-line';
import { Chart } from 'chart.js';

import { ShiftData } from './shift-data';

// import services
import { GetDataService } from './get-data.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  shiftData: ShiftData[];
  iotLines: IotLine[];
  selectedLine: IotLine;
  selectedSection: string;
  selectedEmployee: string;
  processingRateChart: Chart;
  errorPercentageChart: Chart;

  constructor(private _reportData: GetDataService) { }

  ngOnInit() {
    this.iotLines = [{LineId: '1', SectionIds: ['1', '2', '3', '4', '5']}]; // list of lines and their sections
    this.selectedLine = this.iotLines[0];
    this.selectedSection = this.selectedLine.SectionIds[0];
    this.updateData(this.selectedLine.LineId, this.selectedSection, this.selectedEmployee);
  }

  updateData(lineId: string, sectionId: string, employeeId: string) {
    this._reportData.getDataForSelectedParameters(lineId, sectionId, employeeId)
      .subscribe(res => {
        console.log(res);
        this.processingRateChart = new Chart('canvas', {
          type: 'line',
          data: {
            labels: res['timeSlots'], // sample data set - ['5', '6', '7', '8', '9', '10']
            datasets: [{
              label: 'Processing Rate',
              borderColor: 'green',
              data: res['processingRates'], // sample data set - ['75', '85', '90', '95', '85', '78']
              fill: false,
              borderWidth: 1,
              lineTension: 0.2,
            }]
          },
          options: {
            responsive: true,
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });
        this.errorPercentageChart = new Chart('canvas2', {
          type: 'line',
          data: {
            labels: res['timeSlots'], // sample data set - ['5', '6', '7', '8', '9', '10']
            datasets: [{
              label: 'Error Percentage',
              borderColor: 'red',
              data: res['errorPercentages'], // sample data set - ['2', '5', '7', '5', '2', '1']
              fill: false,
              borderWidth: 1,
              lineTension: 0.2,
            }]
          },
          options: {
            responsive: true,
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });
      }
    );
  }

  onLineSelected(iotLine: IotLine) {
    this.selectedLine = iotLine;
    this.selectedSection = iotLine.SectionIds[0];
    this.updateData(this.selectedLine.LineId, this.selectedSection, this.selectedEmployee);
  }

  onSectionSelected(sectionId: string) {
    this.selectedSection = sectionId;
    this.updateData(this.selectedLine.LineId, this.selectedSection, this.selectedEmployee);
  }

  onEmployeeSelected(employeeId: string) {
    this.selectedEmployee = employeeId;
    this.updateData(this.selectedLine.LineId, this.selectedSection, this.selectedEmployee);
  }

}
