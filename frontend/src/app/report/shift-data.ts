export class ShiftData {
    date: Date;
    startTime: Date;
    endTime: Date;
    managerId: string;
    workerId: string;
    managerName: string;
    workerName: string;
    lineId: string;
    sectionId: string;
}
