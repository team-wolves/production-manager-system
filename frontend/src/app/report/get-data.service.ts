import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GetDataService {

  constructor(private _http: HttpClient) { }

  getDataForSelectedParameters(lineId: string, sectionId: string, employeeId: string) {
    return this._http.get('https://jsonplaceholder.typicode.com/todos/1')
      .pipe(map(result => result));
  }
}
