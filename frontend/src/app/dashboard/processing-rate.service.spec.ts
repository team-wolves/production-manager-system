import { TestBed, inject } from '@angular/core/testing';

import { ProcessingRateService } from './processing-rate.service';

describe('ProcessingRateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProcessingRateService]
    });
  });

  it('should be created', inject([ProcessingRateService], (service: ProcessingRateService) => {
    expect(service).toBeTruthy();
  }));
});
