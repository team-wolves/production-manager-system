import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProcessingRateService {

  constructor(private _http: HttpClient) { }

  processingRateForFiveMinitues(lineId: string, sectionId: string) {
    return this._http.get('https://jsonplaceholder.typicode.com/todos/1')
      .pipe(map(result => result)); // https url should be changed after developing the server
  }
}
