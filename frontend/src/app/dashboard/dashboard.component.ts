import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { interval } from 'rxjs';

import { IotLine } from '../iot-line';

// import services
import { ProcessingRateService } from './processing-rate.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  iotLines: IotLine[];
  selectedLine: IotLine;
  selectedSection: string;
  processingRateChart: Chart;
  errorPercentageChart: Chart;
  currentProcessingRate: number;
  currentErrorPercentage: number;

  constructor(private _processingRate: ProcessingRateService) { }

  ngOnInit() {
    this.iotLines = [{LineId: '1', SectionIds: ['1', '2', '3', '4', '5']}]; // list of lines and their sections
    this.selectedLine = this.iotLines[0];
    this.selectedSection = this.selectedLine.SectionIds[0];
    this.updateData(this.selectedLine.LineId, this.selectedSection);
    interval(5000).subscribe(() => { this.updateData(this.selectedLine.LineId, this.selectedSection); });
  }

  updateData(lineId: string, sectionId: string) {
    this._processingRate.processingRateForFiveMinitues(lineId, sectionId)
      .subscribe(res => {
        console.log(res);
        this.currentProcessingRate = res['processingRates'][0] / 60; // divided by 60 to get it per seconds
        this.currentErrorPercentage = res['errorPercentages'][0];
        this.processingRateChart = new Chart('canvas', {
          type: 'line',
          data: {
            labels: res['timeSlots'], // sample data set - ['5', '6', '7', '8', '9', '10']
            datasets: [{
              label: 'Processing Rate',
              borderColor: 'green',
              data: res['processingRates'], // sample data set - ['75', '85', '90', '95', '85', '78']
              fill: false,
              borderWidth: 1,
              lineTension: 0.2,
            }]
          },
          options: {
            responsive: true,
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });
        this.errorPercentageChart = new Chart('canvas2', {
          type: 'line',
          data: {
            labels: res['timeSlots'], // sample data set - ['5', '6', '7', '8', '9', '10']
            datasets: [{
              label: 'Error Percentage',
              borderColor: 'red',
              data: res['errorPercentages'], // sample data set - ['2', '5', '7', '5', '2', '1']
              fill: false,
              borderWidth: 1,
              lineTension: 0.2,
            }]
          },
          options: {
            responsive: true,
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });
      }
    );
  }

  onLineSelected(iotLine: IotLine) {
    this.selectedLine = iotLine;
    this.selectedSection = iotLine.SectionIds[0];
    this.updateData(this.selectedLine.LineId, this.selectedSection);
  }

  onSectionSelected(sectionId: string) {
    this.selectedSection = sectionId;
    this.updateData(this.selectedLine.LineId, this.selectedSection);
  }
}
