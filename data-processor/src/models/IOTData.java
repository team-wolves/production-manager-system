package models;

import java.util.Date;
import java.util.Random;

public class IOTData {

    private Date timestamp;
    private String devId;
    private String lineId;
    private String sectionId;
    private String condition;

    private Random random = new Random();
    public IOTData(Date timestamp, String devId, String condition) {
        this.timestamp = timestamp;
        this.devId = devId;
        String[] lines = {"1", "2", "3", "4", "5"};
        this.lineId = lines[random.nextInt(5)];
        String[] sections = {"one", "two", "three", "four", "five"};
        this.sectionId = sections[random.nextInt(5)];
        this.condition = condition;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getDevId() {
        return devId;
    }

    public void setDevId(String devId) {
        this.devId = devId;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }
}
