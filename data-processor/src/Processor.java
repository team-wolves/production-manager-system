import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.BsonField;
import com.mongodb.client.model.Filters;
import org.bson.*;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import static com.mongodb.client.model.Filters.*;

public class Processor {

    public static void main(String[] args) {
        MongoClient dataWriterClient = new MongoClient();
        MongoDatabase db = dataWriterClient.getDatabase("codefestdb");

        MongoCollection<Document> collection = db.getCollection("iot_data");
        MongoCollection<Document> processDataCollection = db.getCollection("processed_data");
        Calendar minTime = Calendar.getInstance();
        Calendar maxTime = Calendar.getInstance();

        while (true) {
            Date date = new Date();
            minTime.setTime(date);
            minTime.add(Calendar.MINUTE, -2);
            minTime.set(Calendar.SECOND, 0);

            maxTime.setTime(date);
            maxTime.add(Calendar.MINUTE, -1);
            maxTime.set(Calendar.SECOND, 0);

            System.out.println("Looking data between");
            System.out.print(minTime.getTime() + " - ");
            System.out.println(maxTime.getTime());

            AggregateIterable<Document> totalDocsDetails = collection.aggregate(
                    Arrays.asList(
                            Aggregates.match(
                                    and(gt("timestamp", minTime.getTime()), lte("timestamp", maxTime.getTime()))
                            ),
                            new Document("$group",
                                    new Document("_id",
                                            new Document("line_id", "$line_id")
                                                    .append("section_id", "$section_id")
                                                    .append("start_time", minTime.getTime())
                                                    .append("end_time", maxTime.getTime())
                                    )
                                            .append("total", new Document("$sum", 1))
                                            .append("good_count", new Document("$sum",
                                                            new Document("$cond", Document.parse("{if: { $eq: [\"$condition\", \"good\"] }," +
                                                                    "then: 1," +
                                                                    "else: 0" +
                                                                    "} ")
                                                            )
                                                    )
                                            )
                                            .append("bad_count", new Document("$sum",
                                                            new Document("$cond", Document.parse("{if: { $eq: [\"$condition\", \"bad\"] }," +
                                                                    "then: 1," +
                                                                    "else: 0" +
                                                                    "} ")
                                                            )
                                                    )
                                            )
                            )
                    )
            );

            try {
                totalDocsDetails.forEach((Block<? super Document>) document -> {
                    System.out.println(document.toJson());
                    processDataCollection.insertOne(document);
                });
            } catch (NullPointerException e) {
                System.out.println("Error" + e.getMessage());
            }

            try {
                Thread.sleep(1000 * 60);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
    }
}
