import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import models.IOTData;
import org.bson.Document;

import java.util.Date;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

public class DataWriter {

    public static void main(String[] args) {

        MongoClient dataWriterClient = new MongoClient();
        MongoDatabase db = dataWriterClient.getDatabase("codefestdb");

        Scanner sc = new Scanner(System.in);

        System.out.print("writes per sec: ");
        int wps = Integer.parseInt(sc.nextLine());

        int inturptTime = 1000 / wps;
        System.out.println(inturptTime);

        MongoCollection<Document> collection = db.getCollection("iot_data");
        Random random = new Random();

        while (true) {

            System.out.println("writing");
            String devId = UUID.randomUUID().toString();
//            String lineId = UUID.randomUUID().toString();
//            String sectionId = UUID.randomUUID().toString();
            String condition = random.nextBoolean()?"good":"bad";

            IOTData iotData = new IOTData(new Date(), devId,condition);
            collection.insertOne(new Document("timestamp", iotData.getTimestamp())
                    .append("dev_id", iotData.getDevId())
                    .append("line_id", iotData.getLineId())
                    .append("section_id", iotData.getSectionId())
                    .append("condition", iotData.getCondition()));

            try {
                Thread.sleep(inturptTime);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
