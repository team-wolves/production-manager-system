package com.wolves.hirantha;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import org.bson.Document;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;


@WebServlet(name = "ServletData")
public class ServletData extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String s[] = request.getRequestURL().toString().split("/");
        String line = s[4];
        String section = s[5];

        MongoClient client = new MongoClient("127.0.0.1", 27017);
        MongoDatabase db = client.getDatabase("codefestdb");
        MongoCollection collection = db.getCollection("processed_data");


        Calendar currentTime = Calendar.getInstance();
        Calendar hourBeforeTime = Calendar.getInstance();
        hourBeforeTime.add(Calendar.HOUR, -1);

        AggregateIterable results = collection.aggregate(
                Arrays.asList(
                        Aggregates.match(Filters.and(
                                Filters.gt("_id.start_time", hourBeforeTime.getTime()),
                                Filters.lte("_id.end_time", currentTime.getTime()),
                                Filters.eq("_id.line_id", line),
                                Filters.eq("_id.section_id", section)
                                )
                        ),
                        Aggregates.sort(new Document("_id.start_time", -1))
                )
        );

        Gson gson = new Gson();
        PrintWriter pr = response.getWriter();
        JsonParser jp = new JsonParser();
        ArrayList<String> startTimes = new ArrayList<>();
        ArrayList<Integer> total = new ArrayList<>();
        ArrayList<Integer> errors = new ArrayList<>();

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");

        results.forEach((Block<Document>) document -> {
            long date = jp.parse(document.toJson()).getAsJsonObject().get("_id").getAsJsonObject().get("start_time").getAsJsonObject().get("$date").getAsLong();
            startTimes.add(sdf.format(new Date(date)));
            total.add(document.getInteger("total"));
            total.add(document.getInteger("good_count"));
            errors.add(document.getInteger("bad_count"));
        });

        JsonObject object = new JsonObject();
        object.add("startSlots", gson.toJsonTree(startTimes));
        object.add("total", gson.toJsonTree(total));
        object.add("errors", gson.toJsonTree(errors));

        pr.println(object.toString());
    }
}
