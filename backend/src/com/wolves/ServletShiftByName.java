package com.wolves.hirantha;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

@WebServlet(name = "ServletShiftByName")
public class ServletShiftByName extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String s[] = request.getRequestURL().toString().split("/");

        Bson bson = null;

        try {
            String part1 = s[5];
            String part2 = s[6];

            if (part1.equals("manager"))
                bson = Aggregates.match(Filters.eq("manager_id", part2));
            else if (part1.equals("worker"))
                bson = Aggregates.match(Filters.eq("worker_id", part2));

        } catch (ArrayIndexOutOfBoundsException e) {
            bson = Aggregates.sort(new Document("start_time", -1));

        }

        MongoClient client = new MongoClient("127.0.0.1", 27017);
        MongoDatabase db = client.getDatabase("codefestdb");
        MongoCollection collection = db.getCollection("shifts");


        AggregateIterable results = collection.aggregate(
                Arrays.asList(
                        bson,

                        Aggregates.sort(new Document("start_time", -1))
                )
        );


        ArrayList<JsonObject> list = new ArrayList<>();
        JsonParser jsonParser = new JsonParser();
        PrintWriter pr = response.getWriter();
        results.forEach((Block<Document>) document -> {
            list.add(jsonParser.parse(document.toJson()).getAsJsonObject());
        });

        pr.println(new Gson().toJson(list));
    }
}
