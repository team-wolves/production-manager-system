Components
1. Shift Data collection application - Listens to a specified directory and sends updates to a flume agent via RPC protocol. 
2. Shift Data Collection Flume Agent - Listens for RPC calls from the collection application, and pipes them to MongoDB
3. IOT Data Collection Flume Agent - Listens for HTTP calls from IOT devices and, pipes them to MongoDB

Steps to host the ingestion system

1. Compose the ingestion system using docker compose on the `docker-compose.yml`
2. Data collector application has to be run separately (which could be built with maven) 