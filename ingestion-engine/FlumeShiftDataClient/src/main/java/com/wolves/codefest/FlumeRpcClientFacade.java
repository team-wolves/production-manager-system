package com.wolves.codefest;

import org.apache.flume.Event;
import org.apache.flume.EventDeliveryException;
import org.apache.flume.api.RpcClient;
import org.apache.flume.api.RpcClientFactory;
import org.apache.flume.event.EventBuilder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Queue;

public class FlumeRpcClientFacade implements Runnable {
    private Thread thread;
    private RpcClient client;
    private String hostname;
    private int port;
    private int batchSize;
    private Queue<String> dataSource;

    public FlumeRpcClientFacade(String hostname, int port, Queue<String> dataSource, int batchSize) {
        // Setup the RPC connection
        this.hostname = hostname;
        this.port = port;
        this.dataSource = dataSource;
        this.batchSize = batchSize;
    }

    public void sendDataToFlume(ArrayList<String> rawEvents) {
        System.out.println("Sending events to flume" + rawEvents.toString());
        ArrayList<Event> events = new ArrayList<>();
        for(String rawEvent: rawEvents){
            events.add(EventBuilder.withBody(rawEvent, Charset.forName("UTF-8")));
        }

        // Send the event
        try {
            client.appendBatch(events);
        } catch (EventDeliveryException e) {
            // clean up and recreate the client
            client.close();
            client = null;
            System.out.println("Could not send data to flume: " + e.getLocalizedMessage());
        }
    }

    public void cleanUp() {
        // Close the RPC connection
        client.close();
    }

    public void start(){
        if(thread == null){
            thread = new Thread(this, "RPC-Client-Facade");
            thread.start();
        }
    }

    @Override
    public void run() {
        while(true){
            ArrayList<String> events = new ArrayList<>();
            try {
                if (client == null) {
                    client = RpcClientFactory.getDefaultInstance(hostname, port);
                }
                int minBatchSize = client.getBatchSize() < batchSize ? client.getBatchSize() : batchSize;

                int count = 0;
                while (count < minBatchSize) {
                    String event = dataSource.poll();
                    if (event == null) break;
                    events.add(event);
                    count++;
                }
                if (count > 0) sendDataToFlume(events);
                Thread.sleep(1000);
            }catch (Exception ex){
                System.out.println("RPC Client connection failed" + ex.getLocalizedMessage());
                client.close();
                client = null;
            }
        }
    }
}
