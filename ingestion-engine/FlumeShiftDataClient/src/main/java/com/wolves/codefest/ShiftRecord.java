package com.wolves.codefest;

public class ShiftRecord {
    public String date;
    public int startTime;
    public int endTime;
    public String managerId;
    public String workerId;
    public String managerName;
    public String workerName;
    public String lineId;
    public String sectionId;

    public ShiftRecord(String date, int startTime, int endTime, String managerId, String workerId, String managerName, String workerName, String lineId, String sectionId) {
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.managerId = managerId;
        this.workerId = workerId;
        this.managerName = managerName;
        this.workerName = workerName;
        this.lineId = lineId;
        this.sectionId = sectionId;
    }
}
