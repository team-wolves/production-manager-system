package com.wolves.codefest;

import java.nio.file.*;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Main {

    private static Queue<String> data;
    private static FlumeRpcClientFacade flumeRpcClientFacade;

    public static void main(String[] args){
        if(System.getenv("CSV_PATH") == null){
            System.out.println("Please provide CSV_PATH in the environment");
            return;
        }

        String hostname = System.getenv("AGENT_HOST") != null ? System.getenv("AGENT_HOST") : "localhost";
        int port = Integer.parseInt(System.getenv("AGENT_PORT") != null ? System.getenv("AGENT_PORT") : "5065");
        int batchSize = Integer.parseInt(System.getenv("AGENT_BATCH_SIZE") != null ? System.getenv("AGENT_BATCH_SIZE") : "100");

        data = new ConcurrentLinkedQueue<>();
        flumeRpcClientFacade = new FlumeRpcClientFacade(hostname, port, data, batchSize);
        flumeRpcClientFacade.start();

        String lookUpPath = System.getenv("CSV_PATH").endsWith("/") ? System.getenv("CSV_PATH") : System.getenv("CSV_PATH") + "/";

        try (WatchService watchService = FileSystems.getDefault().newWatchService()) {
            Path path = Paths.get(lookUpPath);
            path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);

            WatchKey watchKey;
            while((watchKey = watchService.take()) != null) {
                for(WatchEvent<?> event: watchKey.pollEvents()){
                    Thread.sleep(500);
                    System.out.println(event.kind() + " " + event.context());
                    ContentReader contentReader = new ContentReader(lookUpPath + event.context().toString(), data);
                    contentReader.read();
                }
                watchKey.reset();
            }
        } catch(Exception ex){
            System.out.println(ex.getLocalizedMessage());
            ex.printStackTrace();
        }
    }

}
