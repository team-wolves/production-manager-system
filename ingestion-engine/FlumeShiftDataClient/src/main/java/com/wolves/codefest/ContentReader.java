package com.wolves.codefest;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Queue;

public class ContentReader implements Runnable {
    Thread thread;
    String file;
    Queue<String> data;

    ContentReader(String file, Queue<String> data){
        this.file = file;
        this.data = data;
    }

    public  void read(){
        this.thread = new Thread(this, file + " reader");
        this.thread.start();
    }

    @Override
    public void run() {
        try {
            Thread.sleep(2000); // buffer time
            processFile();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void processFile(){
        try{
            CSVParser csvParser = CSVParser.parse(new File(file), Charset.defaultCharset(), CSVFormat.EXCEL);
            for(CSVRecord csvRecord: csvParser){
                System.out.println("Processing record: " + csvRecord);
                ShiftRecord shiftRecord = new ShiftRecord(
                        csvRecord.get(0), // date
                        Integer.parseInt(csvRecord.get(1)), // start time
                        Integer.parseInt(csvRecord.get(2)), // end time
                        csvRecord.get(3), // manager id
                        csvRecord.get(4), // worker id
                        csvRecord.get(5), // manager name
                        csvRecord.get(6), // worker name
                        csvRecord.get(7), // line id
                        csvRecord.get(8)); // section id
                Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
                String json = gson.toJson(shiftRecord);
                data.add(json);
            }
            System.out.println("Record set processed, and added to queue");
        } catch(FileNotFoundException ex){
            System.out.println("Unable to open the file " + file);
        } catch(IOException ex){
            System.out.println("Error reading the file");
        }
    }
}
