package com.wolves.codefest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.event.EventBuilder;
import org.apache.flume.source.http.HTTPBadRequestException;
import org.apache.flume.source.http.HTTPSourceHandler;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class IotHttpSourceHandler implements HTTPSourceHandler {
    public List<Event> getEvents(HttpServletRequest httpServletRequest) throws HTTPBadRequestException {
        String body = null;
        Gson gson = new Gson();
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;

        try {
            InputStream inputStream = httpServletRequest.getInputStream();
            if(inputStream==null);

            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            char[] charBuffer = new char[128];
            int bytesRead = -1;
            while((bytesRead= bufferedReader.read(charBuffer)) >0){
                stringBuilder.append(charBuffer, 0, bytesRead);
            }
        } catch(IOException ex){
            //TODO add logs
            throw new HTTPBadRequestException("Bad Input Format");
        } finally {
            if(bufferedReader !=null){
                try{
                    bufferedReader.close();
                } catch(IOException ex){
                    //TODO add logs
                    throw new HTTPBadRequestException("Bad Input Format");
                }
            }
        }

        body = stringBuilder.toString();
        if(body.length()==0) throw new HTTPBadRequestException("Empty Body");

        ArrayList<Event> eventsList = new ArrayList<Event>();
        HashMap<String, String> headers = new HashMap<String, String>();

        headers.put("collection", "event_logs");
        eventsList.add(EventBuilder.withBody(body,Charset.forName("utf8"), headers));

        return eventsList;
    }

    public void configure(Context context) {

    }
}
